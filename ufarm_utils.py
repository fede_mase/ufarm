#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 11:37:55 2020

@author: poba
"""

import pandas as pd
from requests import Session
from web3.exceptions import InvalidAddress


def field_selection(dic, fields):

    new_dic = {k: dic[k] for k in fields}

    return new_dic


def quick_get(url):

    with Session() as s:
        try:
            dic = s.get(url).json()
        except Exception as e:
            print('ERRORE: {}'.format(e))
            dic = None

    return dic


def flat_map(list_of_dic, language='en'):

    new_dic = {ob['ID']: ob[language] for ob in list_of_dic}

    return new_dic


def data_process(dic, fields_to_map, dic_map, aggregate=True,
                 unpack_field=False, drop_mapped=False, drop_list=False,
                 rename=False, eth_api=None):

    new_dic = {k: dic[k] for k in dic
               if (k not in fields_to_map) and drop_mapped}
    # map
    for field in fields_to_map:
        new_dic['mapped_{}'.format(field)] = [dic_map[
                field.split('.')[-1]][uid] for uid in dic[field]]

        # aggragate infos
        if aggregate:
            new_dic['n_{}'.format(field)] = len(dic[field])

        if unpack_field:
            fis = pd.Series(new_dic['mapped_{}'.format(field)]).value_counts(
                    ).to_dict()

            new_dic.update(fis)

        if drop_list:
            del new_dic[f'mapped_{field}']

    if eth_api:
        try:
            new_dic['ETHbalance'] = eth_api.eth.getBalance(
                new_dic['Farm.interaction_info.Ethwallet'])/10**18
        except InvalidAddress as e:
            print('Invalid wallet: {}'.format(e))
            new_dic['ETHbalance'] = 0

    # rename field
    if rename:
        new_dic = {k.split('.')[-1]: v for k, v in new_dic.items()}
        if 'name' in new_dic.keys():
            new_dic['garden'] = new_dic.pop('name')

    return new_dic


def extend_data(data, url_keys):

    extended_data = []
    for dic in data:
        new_dic = {k: dic[k] for k in dic if k not in url_keys}
        new_infos = {f'{url_key}.{k}': v for url_key in url_keys
                     for k, v in quick_get(dic[url_key]).items()}
        # further flattening
        new_keys = list(new_infos.keys())
        for info in new_keys:
            if isinstance(new_infos[info], dict):
                new_infos.update(
                    {f'{info}.{k}': v for k, v in new_infos[info].items()})
                del new_infos[info]

        new_dic.update(new_infos)
        extended_data.append(new_dic)

    return extended_data


def make_dataframe(diclist, cols_to_drop=None):

    df = pd.DataFrame().from_dict(diclist)

    if cols_to_drop:
        df = df.drop(columns=cols_to_drop)

    return df


def create_filtered_json(base_url, map_urls, info_url_keys, fields_to_keep,
                         fields_to_map, language_map='en', unpack_field=False,
                         aggregate=True, drop_mapped=True, drop_list=False,
                         rename=False, eth_api=None):
    """This function get all info about all farms in ufarm and filters them
    in a json with desired info elaborated.

    Parameters:

    ------------

    base_url (str): string with endpoint for retrieving basic farm info

    map_urls (dic): dictionary with endpoints for mapping uiid to actual names

    info_url_keys (list): list of the keys of the basic json where are stored
        the endpoints for additional farm info

    fields_to_keep (list): list of the json field you want in the final json

    language_map (str): language for mapping the uiid ['en', 'it', 'de']

    unpack_field (bool): if True create a field for each uuid mapped

    aggregate (bool): if True create a field with aggregated info about mapped
        fields

    drop_mapped (bool): if True drop from the json the fields that have been
        mapped (the ones with original uiid)

    drop_list (bool): if True drop from the json the fields mapped

    Returns:

    ------------

    final_data (list): processed json with desired info
    """

    data = quick_get(base_url)

    data = extend_data(data, info_url_keys)

    dic_maps = {k: flat_map(quick_get(map_urls[k]), language=language_map)
                for k in map_urls}

    data = [field_selection(dic=dic, fields=fields_to_keep) for dic in data]

    data = [data_process(
        dic=dic, fields_to_map=fields_to_map, dic_map=dic_maps,
        aggregate=aggregate, drop_mapped=drop_mapped, drop_list=drop_list,
        rename=rename, eth_api=eth_api)
            for dic in data]

    return data


def json_for_dc(base_url, map_urls, info_url_keys, fields_to_keep,
                fields_to_map, language_map='en', unpack_field=False,
                aggregate=False, drop_mapped=True, drop_list=False,
                fields_to_normalize=['seeds', 'trees', 'UFpeople',
                                     'UFmaxproduction'],
                unpacked_field='productions', eth_api=None):

    data = create_filtered_json(
        base_url, map_urls, info_url_keys, fields_to_keep, fields_to_map,
        language_map, unpack_field, aggregate, drop_mapped, drop_list,
        rename=True, eth_api=eth_api)

    new_data = []

    for dic in data:
        dic = replace_null(dic)
        for frut in dic['productions']:
            new_dic = normalize_data(dic, unpacked_field=unpacked_field,
                                     fields_to_normalize=fields_to_normalize)
            new_dic = {k: new_dic[k] if k != 'productions' else frut
                       for k in new_dic}
            new_data.append(new_dic)

    return new_data


def normalize_data(dic, unpacked_field='productions',
                   fields_to_normalize=['seeds', 'trees', 'UFpeople',
                                        'UFmaxproduction']):

    new_dic = {k: dic[k] for k in dic}
    n = len(dic[unpacked_field])

    for field in fields_to_normalize:
        if new_dic[field]:
            new_dic[field] = new_dic[field]/n

    return new_dic


def replace_null(dic, fields=['UFmaxproduction', 'ETHbalance']):

    for field in fields:
        if dic[field] is None:
            dic[field] = 0

    return dic
