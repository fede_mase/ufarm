#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 12:16:55 2020

@author: poba
"""

import json
import ufarm.ufarm_utils as uu

from web3 import Web3

# =============================================================================
# Test basic functions
# =============================================================================

base_url = "https://ufarm.xyz/ufarm/opendata/catalogue/farms"

map_urls = {'tools': "https://ufarm.xyz/ufarm/opendata/catalogue/tools",
            'productions': "https://ufarm.xyz/ufarm/opendata/catalogue/fruits"}

data = uu.quick_get(base_url)

info_url_keys = ['Farm.registerInfo.geojson', 'Farm.interaction_info',
                 'Farm.interaction_history']

data_extended = uu.extend_data(data, info_url_keys)

dic_maps = {k: uu.flat_map(uu.quick_get(map_urls[k])) for k in map_urls}

base_fields = ['Farm.name', 'Farm.city', 'Farm.county', 'Farm.state',
               'Farm.country']

fields_to_map = ['Farm.interaction_info.productions',
                 'Farm.interaction_info.tools']

fields_to_keep = base_fields + fields_to_map + [
    'Farm.interaction_info.Ethwallet']

data_processed = [uu.data_process(
        farm, fields_to_map=fields_to_map, dic_map=dic_maps, unpack_field=True,
        aggregate=True, drop_mapped=True, drop_list=False)
                  for farm in data_extended]

mapped_cols = [f'mapped_{x}' for x in fields_to_map]

dataset = uu.make_dataframe(data_processed, cols_to_drop=mapped_cols)

# Data filtered

data_filtered = [uu.field_selection(dic, fields_to_keep)
                 for dic in data_extended]

data_filtered = [uu.data_process(
        farm, fields_to_map=fields_to_map, dic_map=dic_maps, unpack_field=True,
        aggregate=True, drop_mapped=True, drop_list=False)
                  for farm in data_filtered]

df_filtered = uu.make_dataframe(data_filtered, mapped_cols)

# =============================================================================
# Test big function
# =============================================================================

with open('conf.json') as f:

    conf = json.load(f)

web3_api = Web3(Web3.HTTPProvider(
    conf['infura_url'].format(conf['infura_key'])))

final_json = uu.create_filtered_json(**conf['oracle'], eth_api=web3_api)

conf['oracle']['aggregate'] = False

new_conf = conf['oracle']
new_conf.update(conf['cs_additional'])

json_for_cs = uu.json_for_dc(**new_conf, eth_api=web3_api)
