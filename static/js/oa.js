function OA(selector) { 
  let OAProto = { elements: document.querySelectorAll(selector) };

  OAProto.on = function(event, cb) { 
    OAProto.elements.forEach(function(e) { e.addEventListener(event, cb) });
  };

  return OAProto;
};

OA.ajax = function(method, url, data, onsuccess, onerror, json) {
  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
      if(xhr.readyState !== XMLHttpRequest.DONE) return;

      if(xhr.status === 200) {
        if(onsuccess) onsuccess(json ? JSON.parse(xhr.responseText) : xhr.responseText);
      }
      else if(onerror) {
        try {
          onerror(JSON.parse(xhr.responseText), xhr.statusText);
        } catch(e) {
          onerror(xhr.responseText, xhr.statusText);
        }
      }
  };

  xhr.open(method, url);

  if(data && !(data instanceof FormData)) {
    if(typeof(data) === "object") {  
      xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
      data = JSON.stringify(data);
    }
  }

  xhr.send(data);
};

OA.ready = function(cb) { document.addEventListener("DOMContentLoaded", cb); }
OA.get = function(url, onsuccess, onerror) { OA.ajax("GET", url, null, onsuccess, onerror); };
OA.json = function(url, onsuccess, onerror) { OA.ajax("GET", url, null, onsuccess, onerror, true); };
OA.put = function(url, data, onsuccess, onerror) { OA.ajax("PUT", url, data, onsuccess, onerror, true); };
OA.delete = function(url, data, onsuccess, onerror) { OA.ajax("DELETE", url, data, onsuccess, onerror, true); };
OA.post = function(url, data, onsuccess, onerror) { OA.ajax("POST", url, data, onsuccess, onerror, true); };
OA.clearElement = function(e) { while(e.firstChild) { e.removeChild(e.firstChild); } };

OA.removeElement = function(e) { 
  if(typeof(e) === "string") e = document.getElementById(e);
  if(e) e.parentElement.removeChild(e);
};

OA.each = function(container, cb) { container.forEach(cb); };
OA.capitalize = function(s) { return s.charAt(0).toUpperCase() + s.slice(1)};

OA.create = function(tagname, obj) {
  let e = document.createElement(tagname);
  if(!obj) return e;

  if(obj.id) e.id = obj.id;

  if(obj.classlist) {
    for(let cn of obj.classlist) e.classList.add(cn);
  }

  if(obj.attributes) {
    for(let attr in obj.attributes) e.setAttribute(attr, obj.attributes[attr]);
  }

  if(obj.dataset) {
    for(let ds in obj.dataset) e.dataset[ds] = obj.dataset[ds];
  }

  if(obj.children) {
    for(let child of obj.children) e.appendChild(child);
  }

  if(obj.html) e.innerHTML += obj.html;
  return e;
};

OA.faIcon = function(icon) { return OA.create("i", { classlist: [ "fa", icon ] }); }; // Deprecated

OA.icon = function(classnames) { 
  let ico = document.createElement("i");
  ico.className = classnames;
  return ico;
};

OA.text = function(text) { return document.createTextNode(text); };

OA.sanitize = function(s) {
  return s.trim().toLowerCase()
         .replace(/[ \t\n\r]+/, " ")
         .replace(/[^a-z0-9_]+/, "_");
};
