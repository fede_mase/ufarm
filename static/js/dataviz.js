function allGraph(jsonData){

var countyRingChart   = new dc.PieChart("#chart-ring-county");
var gardenRowChart = new dc.RowChart("#chart-row-garden");
var countySeedsRingChart   = new dc.PieChart("#chart-ring-seeds-county");
var gardenSeedsRowChart = new dc.RowChart("#chart-row-seeds-garden");
var countyPlantsRingChart   = new dc.PieChart("#chart-ring-plants-county");
var gardenPlantsRowChart = new dc.RowChart("#chart-row-plants-garden");
var productionHistChart  = new dc.BarChart("#chart-hist-production")
                               .yAxisLabel('Estimated production (kg)');

var table = new dc.DataTable('#table');

window.countyRingChart=countyRingChart;
window.gardenRowChart=gardenRowChart;
window.countySeedsRingChart=countySeedsRingChart;
window.gardenSeedsRowChart=gardenSeedsRowChart;
window.countyPlantsRingChart=countyPlantsRingChart;
window.gardenPlantsRowChart=gardenPlantsRowChart;
window.productionHistChart=productionHistChart;
window.tabe=table;

var ndx = crossfilter(jsonData),
    countyDim  = ndx.dimension(function(d) {return d.county;}),
    gardenDim  = ndx.dimension(function(d) {return d.garden;}),
    seedsDim  = ndx.dimension(function(d) {return d.seeds;}),
    seedsDim  = ndx.dimension(function(d) {return d.trees;}),
    productionDim = ndx.dimension(function(d) {return d.productions;}),
    maxProductionDim = ndx.dimension(function(d) {return d.UFmaxproduction;}),
    productionPerCounty = countyDim.group().reduceSum(function(d) {return +d.UFmaxproduction});
    productionPerGarden = gardenDim.group().reduceSum(function(d) {return +d.UFmaxproduction});
    seedsPerCounty = countyDim.group().reduceSum(function(d) {return +d.seeds});
    seedsPerGarden = gardenDim.group().reduceSum(function(d) {return +d.seeds});
    plantsPerCounty = countyDim.group().reduceSum(function(d) {return +d.trees});
    plantsPerGarden = gardenDim.group().reduceSum(function(d) {return +d.trees});
    productionHist = productionDim.group().reduceSum(function(d) {return +d.UFmaxproduction});

countyRingChart
    .dimension(countyDim)
    .group(productionPerCounty)
    .innerRadius(50)
    .controlsUseVisibility(true);

gardenRowChart
    .dimension(gardenDim)
    .group(productionPerGarden)
    .elasticX(true)
    .controlsUseVisibility(true);

countySeedsRingChart
    .dimension(countyDim)
    .colors(d3.scaleOrdinal().domain([-500,500]).range(d3.schemeSet1))
    .group(seedsPerCounty)
    .innerRadius(50)
    .controlsUseVisibility(true);

gardenSeedsRowChart
    .dimension(gardenDim)
    .colors(d3.scaleOrdinal().domain([-500,500]).range(d3.schemeSet1))
    .group(seedsPerGarden)
    .elasticX(true)
    .controlsUseVisibility(true);

countyPlantsRingChart
    .dimension(countyDim)
    .colors(d3.scaleOrdinal().domain([-500,500]).range(d3.schemeSet2))
    .group(plantsPerCounty)
    .innerRadius(50)
    .controlsUseVisibility(true);

gardenPlantsRowChart
    .dimension(gardenDim)
    .colors(d3.scaleOrdinal().domain([-500,500]).range(d3.schemeSet2))
    .group(plantsPerGarden)
    .elasticX(true)
    .controlsUseVisibility(true);

productionHistChart
    .height(400)
    .margins({left: 75, top: 10, right: 10, bottom: 140})
    .colors(d3.scaleOrdinal().domain([-500,500]).range(d3.schemePaired))
    .dimension(productionDim)
    .group(productionHist)
    .elasticY(true)
    .x(d3.scaleBand())
    .xUnits(dc.units.ordinal)
    .controlsUseVisibility(true);

//productionHistChart.yAxis().ticks(3);

  function show_empty_message(chart) {
      var is_empty = d3.sum(chart.group().all().map(chart.valueAccessor())) === 0;
      var data = is_empty ? [1] : [];
      var empty = chart.svg().selectAll('.empty-message').data(data);
      empty.exit().remove();
      empty = empty
          .enter()
              .append('text')
              .text('NO DATA!')
              .attr('text-anchor', 'middle')
              .attr('alignment-baseline', 'middle')
              .attr('class', 'empty-message')
              .attr('x', chart.margins().left + chart.effectiveWidth()/2)
              .attr('y', chart.margins().top + chart.effectiveHeight()/2)
              .style('opacity', 0)
          .merge(empty);
      empty.transition().duration(1000).style('opacity', 1);
  }

  productionHistChart.on('pretransition', show_empty_message);
  gardenRowChart.on('pretransition', show_empty_message);
  gardenPlantsRowChart.on('pretransition',show_empty_message);
  gardenSeedsRowChart.on('pretransition',show_empty_message);


table
    .dimension(gardenDim)
    .sortBy(function(d) { return d.garden; })
    .showSections(false)
    .size(Infinity)
    .columns(['garden',
              {
                  label: 'Product',
                  format: function(d) {
                      return d.productions;
                  }      
              },
              {
              label: 'Extimated production (kg)',
              format: function(d){
                      return d.UFmaxproduction.toFixed(2);
                      }
              },          
              'county',
              {
              label: 'Seeds',
              format: function(d){
                      return d.seeds.toFixed(4);
                      }
              },
              {
              label: 'Plants',
              format: function(d){
                      return d.trees.toFixed(4);
                      }
              },
              {
              label: 'Wallet address',
              format: function(d){
                      return d.Ethwallet;
                      }
              },
              {
              label: 'Wallet balance (ETH)',
              format: function(d){
                      return d.ETHbalance.toFixed(6);
                      }
              }
            ]);


d3.select('#download')
    .on('click', function() {
        var data = gardenDim.top(Infinity);
        if(d3.select('#download-type input:checked').node().value==='table') {
            data = data.sort(function(a, b) {
                return table.order()(table.sortBy()(a), table.sortBy()(b));
            });
            data = data.map(function(d) {
                var row = {};
                table.columns().forEach(function(c) {
                    row[table._doColumnHeaderFormat(c)] = table._doColumnValueFormat(c, d);
                });
                return row;
            });
        }
        var blob = new Blob([d3.csvFormat(data)], {type: "text/csv;charset=utf-8"});
        saveAs(blob, 'data.csv');
    });

dc.renderAll();

}

OA.json("/cs_json",function(data){
  window.dataviz=data;  
  allGraph(window.dataviz);
});