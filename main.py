import json

from flask import Flask, render_template, send_from_directory, jsonify
from flask import make_response
from ufarm_utils import create_filtered_json, json_for_dc
from web3 import Web3

with open('conf.json') as f:
    conf = json.load(f)

web3_api = Web3(
    Web3.HTTPProvider(conf['infura_url'].format(conf['infura_key'])))


app = Flask(__name__)
# w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545"))
@app.route('/docs/<path:path>')
def send_pdf(path):
    return send_from_directory('static/docs/', path)


@app.route('/dist/<path:path>')
def send_dist(path):
    return send_from_directory('static/dist', path)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('static/img', path)


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/api")
def my_api():

    try:
        res = jsonify(create_filtered_json(
            **conf['oracle'],
            eth_api=web3_api if web3_api.isConnected() else None))
    except Exception as e:
        print(f'ERROR: {e}')
        res = make_response('error', 500)

    return res


@app.route("/cs_json")
def cs_json():

    cazzeconf = {k: v if k != 'aggregate' else False
                 for k, v in conf['oracle'].items()}

    cazzeconf.update(conf['cs_additional'])

    try:
        res = jsonify(json_for_dc(
            **cazzeconf,
            eth_api=web3_api if web3_api.isConnected() else None))

    except Exception as e:
        print(f'ERROR: {e}')
        res = make_response('error', 500)

    return res


if __name__ == "__main__":

    app.run("0.0.0.0")
